from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from blog.sitemaps import BlogSitemap
from blog.views import PostListView

admin.autodiscover()

sitemaps = {
    'blog': BlogSitemap()
}


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^blog/', include('blog.urls')),
    url(r'^$', PostListView.as_view(template_name='blog/post_list_with_content.html'), name='home'),

    url(r'^markitup/', include('markitup.urls')),
    # Uncomment the next line to enable the admin:

    (r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps})
)
