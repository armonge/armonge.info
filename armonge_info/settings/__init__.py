# -*- coding: utf-8 -*-
from __future__ import absolute_import

from armonge_info.settings.settings import *

try:
    from armonge_info.settings.local import *
except ImportError:
    pass