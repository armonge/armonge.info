# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Post.tags'
        db.delete_column('blog_post', 'tags')

        # Adding field 'Post.tag_list'
        db.add_column('blog_post', 'tag_list',
                      self.gf('tagging.fields.TagField')(default=''),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Post.tags'
        db.add_column('blog_post', 'tags',
                      self.gf('tagging.fields.TagField')(default=''),
                      keep_default=False)

        # Deleting field 'Post.tag_list'
        db.delete_column('blog_post', 'tag_list')


    models = {
        'blog.post': {
            'Meta': {'object_name': 'Post'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'tag_list': ('tagging.fields.TagField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['blog']