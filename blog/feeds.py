# -*- coding: utf-8 -*-

from django.contrib.syndication.views import Feed
from django.shortcuts import get_object_or_404
from tagging.models import Tag, TaggedItem
from blog.models import Post
from django.utils.feedgenerator import Atom1Feed

class RssSiteNewsFeed(Feed):
    title = "armonge"
    link = "/blog/"
    description = ""

    def items(self, obj=None):
        queryset = Post.published.all().order_by('-date_published')

        if obj is not None:
            queryset = TaggedItem.objects.get_by_model(queryset, obj)

        return queryset[:5]

    def get_object(self, request, tag=None, *args, **kwargs):
        if tag is not None:
            return get_object_or_404(Tag, name=tag)
        super(RssSiteNewsFeed, self).get_object(request, *args, **kwargs)


class AtomSiteNewsFeed(RssSiteNewsFeed):
    feed_type = Atom1Feed
    subtitle = RssSiteNewsFeed.description
