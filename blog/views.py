from django.http import Http404
from django.views.generic import ListView, DetailView
from tagging.models import TaggedItem
from blog.models import Post

class PostDetailView(DetailView):
    model = Post

class PostListView(ListView):
    model = Post
    queryset = Post.published.all()

    def get_queryset(self):
        queryset = super(PostListView, self).get_queryset()

        if self.kwargs.get('tag'):
            queryset = TaggedItem.objects.get_by_model(queryset, self.kwargs['tag'])
            if not queryset:
                raise Http404

        return queryset
