# -*- coding: utf-8 -*-

from django.contrib import admin
from django.db import models
from blog.models import Post
from markitup.widgets import AdminMarkItUpWidget

class PostAdmin(admin.ModelAdmin):
    list_filter = ('tag_list',)
    prepopulated_fields = {
        'slug': ('name',),
    }
    formfield_overrides = {
        models.TextField: {'widget': AdminMarkItUpWidget}
    }



admin.site.register(Post, PostAdmin)