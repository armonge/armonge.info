import datetime
from django.db import models
from tagging.fields import TagField
from tagging.models import Tag
from model_utils.managers import QueryManager

class Post(models.Model):
    name = models.CharField(max_length=200, verbose_name=u'nombre')
    slug = models.SlugField()
    text = models.TextField(verbose_name=u'texto')
    date_published = models.DateTimeField(default=datetime.datetime.now)

    tag_list = TagField()
    public = models.BooleanField()

    objects = models.Manager()
    published = QueryManager(public=True)

    @models.permalink
    def get_absolute_url(self):
        return ('blog_post_detail', (),{
            'slug': self.slug,
            'pk': self.pk
        })

    def __unicode__(self):
        return self.name


    def save(self):
        super(Post, self).save()
        self.tags = self.tag_list

    def _get_tags(self):
        return Tag.objects.get_for_object(self)

    def _set_tags(self, tag_list):
        Tag.objects.update_tags(self, tag_list)

    tags = property(_get_tags, _set_tags)


    class Meta:
        ordering = ('-date_published',)