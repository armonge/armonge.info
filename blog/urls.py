# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from blog.views import PostDetailView, PostListView
from blog.feeds import RssSiteNewsFeed, AtomSiteNewsFeed


urlpatterns = patterns('blog.views',
    (r'^rss/$', RssSiteNewsFeed()),
    (r'^rss/(?P<tag>[\w-]+)/$', RssSiteNewsFeed()),
    (r'^atom/$', AtomSiteNewsFeed()),
    url(r'^(?P<pk>\d+)/(?P<slug>[\w-]+)/$', PostDetailView.as_view(), name='blog_post_detail'),
    url(r'^(?P<tag>[\w-]+)/$', PostListView.as_view(), name='blog_post_list_by_tag'),
    url(r'^$',PostListView.as_view(), name='blog_post_list'),
)
